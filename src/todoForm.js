import { useState } from "react";

const TodoForm = ({ addTodo }) => {
    const [value, setValue] = useState("");
    const handleSubmit = ((e) => {
        e.preventDefault();
        addTodo(value)
        setValue("");
    })

    return (
        <div className="todo-Form">
            <form onSubmit={handleSubmit}>
                <input type="text" value={value} onChange={e => setValue(e.target.value)} required />
                <button onClick={()=>handleSubmit}>+</button>
            </form>
         
        </div>
    );
}

export default TodoForm;