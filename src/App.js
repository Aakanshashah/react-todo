import { useState } from 'react';
import './App.css';
import Todo from './todo';
import TodoForm from './todoForm';

function App() {

  const [todos, setTodos] = useState([
    {
      text: "Try to complete task",
      isCompleted: false
    },
    {
      text: "Learn about React",
      isCompleted: false
    },
    {
      text: "Meet friend for lunch",
      isCompleted: false
    },
    {
      text: "Build  todo app",
      isCompleted: false
    }
  ]);

  const addTodo = text => {
    const newTodo = [...todos, { text }];
      setTodos(newTodo);
  }
  const completeTodo = index => {
    const newTodo = [...todos];
    newTodo[index].isCompleted = true;
    setTodos(newTodo);
  }
  const unCompleTodo = index => {
    const newTodo = [...todos];
    newTodo[index].isCompleted = false;
    setTodos(newTodo);
  }
  const removeTodo = index => {
    const newTodo = [...todos];
    newTodo.splice(index, 1)
    setTodos(newTodo);
  }


  return (
    <div className="app">
      <h1>Todo list</h1>
      <TodoForm addTodo={addTodo} />
      <div className="todo-list">
        {todos.map((todo, index) => (
          <div className="todolist">
           <Todo completeTodo={completeTodo} unCompleTodo={unCompleTodo} key={index} index={index} todo={todo} removeTodo={removeTodo} />
          </div>
        ))}

      </div>
    </div>
  );
}

export default App;