const Todo = ({ completeTodo, index, todo, removeTodo ,unCompleTodo}) => {
  
    return (
        <div className="todo" style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}>
            {todo.text}
            <div className="container">
                <button onClick={() => completeTodo(index)} onDoubleClick={()=>unCompleTodo(index)}>complete</button>
                <button onClick={()=>unCompleTodo(index)}>incomplete</button>
                <button onClick={() => removeTodo(index)} >delete</button>
            </div>

        </div>
    );
}

export default Todo;